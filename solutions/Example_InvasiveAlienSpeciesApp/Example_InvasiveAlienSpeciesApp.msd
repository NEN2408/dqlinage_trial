<?xml version="1.0" encoding="UTF-8"?>
<solution name="Example_InvasiveAlienSpeciesApp" uuid="cddc8df6-098f-4b73-a1e2-7273103fdf5c" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">fab11043-404a-46cc-b039-37dc7a2d7229(CI_Package)</dependency>
    <dependency reexport="false">f512ebed-7d7b-4f91-9e6c-7ea0170d5f18(DQLineage)</dependency>
    <dependency reexport="false">782cfd93-7b8c-49eb-b1bf-b438cac2f1b0(DQLineage.dates)</dependency>
    <dependency reexport="false">74b08d39-3283-475c-8194-d69c8f9f5f4b(ProjectDocumentation)</dependency>
    <dependency reexport="false">0b7f2717-8194-426b-b8e0-ecc86b8eb42b(RS_Package)</dependency>
  </dependencies>
  <languageVersions />
  <dependencyVersions>
    <module reference="cddc8df6-098f-4b73-a1e2-7273103fdf5c(Example_InvasiveAlienSpeciesApp)" version="0" />
  </dependencyVersions>
</solution>

